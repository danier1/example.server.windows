﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Example.DataAccess.Configuration;
using Example.DataAccess.Core;
using Example.DataAccess.Repositories;
using Example.Domain.Entities;
using Example.Manager.Managers;

namespace Example.TestConsole
{
    class Program
    {
        static void Main()
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<ExampleDbContext>());

            DbContext dbContext = new ExampleDbContext();
            IUnitOfWork unitOfWork = new UnitOfWork(dbContext);
            ICategoriesRepository categoriesRepository = new CategoriesRepository(dbContext);
            IProductsRepository productsRepository = new ProductsRepository(dbContext);
            ICategoriesManager categoriesManager = new CategoriesManager(categoriesRepository, unitOfWork);
            IProductsManager productsManager = new ProductsManager(productsRepository, unitOfWork);

            Category category = categoriesManager.First(c => c.Id == new Guid("04ecf593-788e-e311-8f42-50465de5ebf6"), "Image");
            categoriesManager.Delete(category);
        }
    }
}
