﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Example.Domain.Entities;

namespace Example.DataAccess.Mappings
{
    public class ProductImageMap : EntityTypeConfiguration<ProductImage>
    {
        public ProductImageMap()
        {
            HasKey(i => i.Id);

            Property(i => i.ImageData).IsRequired();
            Property(i => i.ImageMimeType).IsRequired().HasMaxLength(8);

            HasRequired(i => i.Product).WithRequiredDependent(p => p.Image).WillCascadeOnDelete(true);
        }
    }
}