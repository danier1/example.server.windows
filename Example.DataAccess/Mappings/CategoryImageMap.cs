﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Example.Domain.Entities;

namespace Example.DataAccess.Mappings
{
    public class CategoryImageMap : EntityTypeConfiguration<CategoryImage>
    {
        public CategoryImageMap()
        {
            HasKey(i => i.Id);
            
            Property(i => i.ImageData).IsRequired();
            Property(i => i.ImageMimeType).IsRequired().HasMaxLength(8);

            HasRequired(i => i.Category).WithRequiredDependent().WillCascadeOnDelete(true);
        }
    }
}