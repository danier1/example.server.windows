﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Example.Domain.Entities;

namespace Example.DataAccess.Mappings
{
    public class ProductMap : EntityTypeConfiguration<Product>
    {
        public ProductMap()
        {
            HasKey(p => p.Id);
            Property(p => p.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(p => p.RowVersion).IsRowVersion();

            Property(p => p.Name).IsRequired().HasMaxLength(64);
            Property(p => p.Description).IsRequired().HasMaxLength(256);
            Property(p => p.Quantity).IsRequired();
            Property(p => p.Price).IsRequired();

            HasRequired(p => p.Image).WithRequiredPrincipal().WillCascadeOnDelete(true);
            
            HasRequired(p => p.Category).WithMany().HasForeignKey(p => p.CategoryId).WillCascadeOnDelete(true);
        }
    }
}