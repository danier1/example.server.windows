﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Example.Domain.Entities;

namespace Example.DataAccess.Mappings
{
    public class CategoryMap : EntityTypeConfiguration<Category>
    {
        public CategoryMap()
        {
            HasKey(c => c.Id);
            Property(c => c.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(c => c.RowVersion).IsRowVersion();

            Property(c => c.Name).IsRequired().HasMaxLength(64);
            Property(c => c.Description).IsRequired().HasMaxLength(256);

            HasRequired(c => c.Image).WithRequiredPrincipal().WillCascadeOnDelete(true);

            HasMany(c => c.Products).WithRequired().HasForeignKey(p => p.CategoryId).WillCascadeOnDelete(true);
            HasMany(c => c.SubCategories).WithOptional(c => c.ParentCategory).HasForeignKey(c => c.ParentCategoryId);
        }
    }
}