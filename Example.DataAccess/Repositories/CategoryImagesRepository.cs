﻿using System;
using System.Data.Entity;
using Example.DataAccess.Core;
using Example.Domain.Entities;

namespace Example.DataAccess.Repositories
{
    public class CategoryImagesRepository : BaseRepository<CategoryImage, Guid>, ICategoryImagesRepository
    {
        public CategoryImagesRepository(DbContext dbContext) : base(dbContext)
        {
        }
    }
}