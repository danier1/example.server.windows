﻿using System;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Example.DataAccess.Core;
using Example.Domain.Entities;

namespace Example.DataAccess.Repositories
{
    public class CategoriesRepository : BaseRepository<Category, Guid>, ICategoriesRepository
    {
        public CategoriesRepository(DbContext dbContext) : base(dbContext)
        {
        }

        public async Task RecursiveDeleteAsync(Guid categoryId)
        {
            await DbContext.Database.ExecuteSqlCommandAsync("Category_Delete @CategoryId",
                new SqlParameter("@CategoryId", categoryId));
        }
    }
}