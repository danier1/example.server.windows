﻿using System;
using Example.DataAccess.Core;
using Example.Domain.Entities;

namespace Example.DataAccess.Repositories
{
    public interface IProductImagesRepository : IRepository<ProductImage, Guid>
    {
    }
}