﻿using System;
using System.Threading.Tasks;
using Example.DataAccess.Core;
using Example.Domain.Entities;

namespace Example.DataAccess.Repositories
{
    public interface ICategoriesRepository : IRepository<Category, Guid>
    {
        Task RecursiveDeleteAsync(Guid categoryId);
    }
}