﻿using System;
using System.Data.Entity;
using Example.DataAccess.Core;
using Example.Domain.Entities;

namespace Example.DataAccess.Repositories
{
    public class ProductImagesRepository : BaseRepository<ProductImage, Guid>, IProductImagesRepository
    {
        public ProductImagesRepository(DbContext dbContext) : base(dbContext)
        {
        }
    }
}