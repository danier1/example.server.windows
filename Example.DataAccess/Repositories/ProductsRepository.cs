﻿using System;
using System.Data.Entity;
using Example.DataAccess.Core;
using Example.Domain.Entities;

namespace Example.DataAccess.Repositories
{
    public class ProductsRepository : BaseRepository<Product, Guid>, IProductsRepository
    {
        public ProductsRepository(DbContext dbContext) : base(dbContext)
        {
        }
    }
}