namespace Example.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeProductIdentityGen : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ProductImages", "Id", c => c.Guid(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ProductImages", "Id", c => c.Guid(nullable: false, identity: true));
        }
    }
}
