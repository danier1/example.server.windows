namespace Example.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCategoryDeleteStoreProcedure : DbMigration
    {
        public override void Up()
        {
            CreateStoredProcedure(
                "dbo.Category_Delete",
                p => new
                {
                    CategoryId = p.Guid()
                },
                body:
                    @"	SET NOCOUNT ON;

                        BEGIN TRANSACTION 
                        BEGIN TRY 
                            DECLARE @ChildCats TABLE 
                            ( 
                                CatId uniqueidentifier 
                            ) 

							DELETE FROM Products WHERE CategoryId = @CategoryId
        
                            DECLARE @ChildCatId uniqueidentifier        
        
                            INSERT INTO @ChildCats 
                            SELECT Id FROM Categories WHERE ParentCategoryId = @CategoryId
        
                            WHILE EXISTS (SELECT CatId FROM @ChildCats) 
                            BEGIN 
                                SELECT TOP 1 
                                    @ChildCatId = CatId 
                                FROM 
                                    @ChildCats 
                
                                DELETE FROM @ChildCats WHERE CatId = @ChildCatId 
            
                                EXEC Category_Delete @ChildCatId 
                            END            
                
                            DELETE FROM dbo.[Categories] 
                            WHERE Id = @CategoryId
        
                            COMMIT TRANSACTION 
                        END TRY 
                        BEGIN CATCH 
                            ROLLBACK TRANSACTION 
						END CATCH");
        }
        
        public override void Down()
        {
            DropStoredProcedure("dbo.Category_Delete");
        }
    }
}
