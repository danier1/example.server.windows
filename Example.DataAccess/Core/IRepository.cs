﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Example.Domain.Core;

namespace Example.DataAccess.Core
{
    public interface IRepository<TEntity, in TKey> where TEntity : BaseEntity<TKey>
    {
        void Add(TEntity entity);

        void Update(TEntity entity);

        void Delete(TEntity entity);

        TEntity Find(TKey key);
        Task<TEntity> FindAsync(TKey key);
        Task<TEntity> FindAsync(TKey key, CancellationToken cancellationToken);

        IQueryable<TEntity> Where(Expression<Func<TEntity, bool>> criteria, params string[] includes);

        IQueryable<TEntity> FindAll(params string[] includes);

        int Count();
        Task<int> CountAsync();
        Task<int> CountAsync(CancellationToken cancellationToken);
    }
}