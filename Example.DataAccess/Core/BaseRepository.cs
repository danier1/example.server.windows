﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Example.Domain.Core;

namespace Example.DataAccess.Core
{
    public class BaseRepository<TEntity, TKey> : IRepository<TEntity, TKey> where TEntity : BaseEntity<TKey>
    {
        protected readonly DbContext DbContext;
        protected readonly DbSet<TEntity> DbSet;

        public BaseRepository(DbContext dbContext)
        {
            if (dbContext == null)
            {
                throw new ArgumentNullException("dbContext");
            }

            DbContext = dbContext;
            DbSet = DbContext.Set<TEntity>();
        }

        public void Add(TEntity entity)
        {
            DbSet.Add(entity);
        }

        public void Update(TEntity entity)
        {
            DbContext.Entry(entity).State = EntityState.Modified;
        }

        public void Delete(TEntity entity)
        {
            DbSet.Remove(entity);
        }

        public TEntity Find(TKey key)
        {
            return DbSet.Find(key);
        }

        public async Task<TEntity> FindAsync(TKey key)
        {
            return await DbSet.FindAsync(key);
        }

        public async Task<TEntity> FindAsync(TKey key, CancellationToken cancellationToken)
        {
            return await DbSet.FindAsync(cancellationToken, key);
        }

        public IQueryable<TEntity> Where(Expression<Func<TEntity, bool>> criteria, params string[] includes)
        {
            return FindAll(includes).Where(criteria);
        }

        public IQueryable<TEntity> FindAll(params string[] includes)
        {
            return includes.Aggregate(DbSet, (Func<DbQuery<TEntity>, string, DbQuery<TEntity>>)
                ((current, include) => current.Include(include)));
        }

        public int Count()
        {
            return DbSet.Count();
        }

        public async Task<int> CountAsync()
        {
            return await DbSet.CountAsync();
        }

        public async Task<int> CountAsync(CancellationToken cancellationToken)
        {
            return await DbSet.CountAsync(cancellationToken);
        }
    }
}