﻿using System.Threading;
using System.Threading.Tasks;

namespace Example.DataAccess.Core
{
    public interface IUnitOfWork
    {
        void Commit();
        Task CommitAsync();
        Task CommitAsync(CancellationToken cancellationToken);
    }
}