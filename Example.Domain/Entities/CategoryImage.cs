﻿using System;
using Example.Domain.Core;

namespace Example.Domain.Entities
{
    public class CategoryImage : BaseEntity<Guid>
    {
        public byte[] ImageData { get; set; }
        public string ImageMimeType { get; set; }

        public virtual Category Category { get; set; }
    }
}