﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Example.Core.Enums;
using Example.Domain.Core;

namespace Example.Domain.Entities
{
    public class Category : BaseEntityWithRowVersion<Guid>
    {
        public Category()
        {
            Products = new Collection<Product>();
            SubCategories = new Collection<Category>();
        }

        public string Name { get; set; }
        public string Description { get; set; }

        public ChildrenType ChildrenType { get; set; }

        public virtual CategoryImage Image { get; set; }

        public Guid? ParentCategoryId { get; set; }
        public virtual Category ParentCategory { get; set; }

        public virtual ICollection<Product> Products { get; set; }

        public virtual ICollection<Category> SubCategories { get; set; }
    }
}