﻿using System;
using Example.Domain.Core;

namespace Example.Domain.Entities
{
    public class ProductImage : BaseEntity<Guid>
    {
        public byte[] ImageData { get; set; }
        public string ImageMimeType { get; set; }

        public virtual Product Product { get; set; }
    }
}