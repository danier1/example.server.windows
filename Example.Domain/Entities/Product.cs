﻿using System;
using Example.Domain.Core;

namespace Example.Domain.Entities
{
    public class Product : BaseEntityWithRowVersion<Guid>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }

        public virtual ProductImage Image { get; set; }

        public Guid CategoryId { get; set; }
        public virtual Category Category { get; set; }
    }
}