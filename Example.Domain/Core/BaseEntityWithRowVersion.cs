﻿namespace Example.Domain.Core
{
    public abstract class BaseEntityWithRowVersion<TKey> : BaseEntity<TKey>
    {
        #region Public properties

        public byte[] RowVersion { get; set; }

        #endregion
    }
}