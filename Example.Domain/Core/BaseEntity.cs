﻿using System;

namespace Example.Domain.Core
{
    public abstract class BaseEntity<TKey> : IEquatable<BaseEntity<TKey>>
    {
        #region Public properties

        public TKey Id { get; set; }
        
        #endregion

        #region IEquatable<BaseEntity<TKey>> Member

        public bool Equals(BaseEntity<TKey> other)
        {
            if (other == null)
            {
                return false;
            }

            if (ReferenceEquals(this, other))
            {
                return true;
            }

            if (!IsTransient(this) && !IsTransient(other) && Equals(Id, other.Id))
            {
                Type thisType = GetUnproxiedType();
                Type otherType = GetUnproxiedType();

                return thisType.IsAssignableFrom(otherType) ||
                       otherType.IsAssignableFrom(thisType);
            }

            return false;
        }

        #endregion

        #region Override Object method's

        public override bool Equals(object obj)
        {
            return Equals(obj as BaseEntity<TKey>);
        }

        public override int GetHashCode()
        {
            if (Equals(Id, default(TKey)))
            {
                return base.GetHashCode();
            }

            return Id.GetHashCode();
        }

        #endregion

        #region Private methods

        private bool IsTransient(BaseEntity<TKey> other)
        {
            return other != null && Equals(other.Id, default(TKey));
        }

        private Type GetUnproxiedType()
        {
            return GetType();
        }

        #endregion
    }
}