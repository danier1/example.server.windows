﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.OData.Query;
using System.Web.Mvc;
using Example.Core.Exceptions;
using Example.Domain.Entities;
using Example.Manager.Managers;
using Example.RestServices.Mappers;
using Example.RestServices.Models;

namespace Example.RestServices.Controllers
{
    public class CategoriesController : ApiController
    {
        private readonly ICategoriesManager _categoriesManager;
        private readonly IMapper _mapper;

        public CategoriesController(IMapper mapper, ICategoriesManager categoriesManager)
        {
            _mapper = mapper;
            _categoriesManager = categoriesManager;
        }

        [ResponseType(typeof (IList<CategoryModel>))]
        public async Task<IHttpActionResult> GetCategories(ODataQueryOptions<Category> query,
            Guid? parentCategoryId = null)
        {
            var queryCategories = query
                .ApplyTo(_categoriesManager.Where(c => c.ParentCategoryId == parentCategoryId))
                as IQueryable<Category>;

            return Ok((await queryCategories.ToListAsync())
                .Select(category => _mapper.Map<Category, CategoryModel>(category)).ToList());
        }

        [ResponseType(typeof (CategoryModel))]
        public async Task<IHttpActionResult> GetCategory(Guid id)
        {
            Category category = await _categoriesManager.FindAsync(id);

            if (category == null)
            {
                return NotFound();
            }

            CategoryModel categoryModel = _mapper.Map<Category, CategoryModel>(category);

            return Ok(categoryModel);
        }

        [ResponseType(typeof (CategoryResultModel))]
        public async Task<IHttpActionResult> PutCategory(Guid id, CategoryModel categoryModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != categoryModel.Id)
            {
                return BadRequest();
            }

            Category category = await _categoriesManager.FindAsync(id);

            if (category == null)
            {
                return NotFound();
            }

            if (!StructuralComparisons.StructuralEqualityComparer
                .Equals(category.RowVersion, categoryModel.RowVersion))
            {
                return StatusCode(HttpStatusCode.Conflict);
            }

            _mapper.Map(categoryModel, category);

            try
            {
                await _categoriesManager.UpdateAsync(category);
            }
            catch (DatabaseException ex)
            {
                if (ex.InnerException is DbUpdateConcurrencyException)
                {
                    category = _categoriesManager.Find(id);

                    if (category == null)
                    {
                        return NotFound();
                    }

                    return StatusCode(HttpStatusCode.Conflict);
                }

                throw;
            }

            CategoryResultModel resultModel = _mapper.Map<Category, CategoryResultModel>(category);

            return Ok(resultModel);
        }

        [ResponseType(typeof (CategoryResultModel))]
        public async Task<IHttpActionResult> PostCategory(Category category)
        {
            await _categoriesManager.AddAsync(category);

            CategoryResultModel resultModel = _mapper.Map<Category, CategoryResultModel>(category);

            return Ok(resultModel);
        }

        public async Task<IHttpActionResult> DeleteCategory(Guid id)
        {
            Category category = await _categoriesManager.FindAsync(id);

            if (category == null)
            {
                return NotFound();
            }

            await _categoriesManager.RecursiveDeleteAsync(category.Id);

            return Ok();
        }
    }
}