﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;
using Example.Domain.Entities;
using Example.Manager.Managers;

namespace Example.RestServices.Controllers
{
    public class CategoryImagesController : ApiController
    {
        private readonly ICategoriesManager _categoriesManager;
        private readonly ICategoryImagesManager _categoryImagesManager;

        public CategoryImagesController(ICategoryImagesManager categoryImagesManager,
            ICategoriesManager categoriesManager)
        {
            _categoryImagesManager = categoryImagesManager;
            _categoriesManager = categoriesManager;
        }

        public async Task<IHttpActionResult> PostCategoryImage()
        {
            if (!Request.Content.IsMimeMultipartContent())
            {
                return StatusCode(HttpStatusCode.UnsupportedMediaType);
            }

            var provider = new MultipartMemoryStreamProvider();
            await Request.Content.ReadAsMultipartAsync(provider);

            CategoryImage image = null;
            Guid categoryId = Guid.Empty;

            foreach (HttpContent attr in provider.Contents)
            {
                string attrName = attr.Headers.ContentDisposition.Name.Trim('\"');

                switch (attrName)
                {
                    case "categoryId":
                        string categoryIdString = await attr.ReadAsStringAsync();

                        categoryId = new Guid(categoryIdString);

                        if (categoryId == Guid.Empty)
                        {
                            return BadRequest(categoryIdString);
                        }

                        break;
                    case "file":
                        string filename = attr.Headers.ContentDisposition.FileName.Trim('\"');
                        string extension = Path.GetExtension(filename);

                        if (extension.Length > 8)
                        {
                            return BadRequest(extension);
                        }

                        byte[] buffer = await attr.ReadAsByteArrayAsync();

                        image = new CategoryImage
                        {
                            ImageData = buffer,
                            ImageMimeType = extension
                        };

                        break;
                    default:
                        return BadRequest();
                }
            }


            if (categoryId == Guid.Empty || image == null)
            {
                return BadRequest("Wrong params");
            }

            Category category = await _categoriesManager.FindAsync(categoryId);

            if (category == null)
            {
                return NotFound();
            }

            image.Category = category;

            await _categoryImagesManager.AddAsync(image);

            return Ok();
        }

        public async Task<HttpResponseMessage> GetCategoryImages(Guid id)
        {
            CategoryImage categoryImage = await _categoryImagesManager.FindAsync(id);

            if (categoryImage == null)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }

            var ms = new MemoryStream(categoryImage.ImageData);
            string mediaType = string.Format("image/{0}", categoryImage.ImageMimeType);

            var response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new StreamContent(ms);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue(mediaType);

            return response;
        }
    }
}