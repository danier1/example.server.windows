﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.OData.Query;
using Example.Core.Exceptions;
using Example.Domain.Entities;
using Example.Manager.Managers;
using Example.RestServices.Mappers;
using Example.RestServices.Models;

namespace Example.RestServices.Controllers
{
    public class ProductsController : ApiController
    {
        private readonly ICategoriesManager _categoriesManager;
        private readonly IMapper _mapper;
        private readonly IProductsManager _productsManager;

        public ProductsController(IMapper mapper, IProductsManager productsManager,
            ICategoriesManager categoriesManager)
        {
            _mapper = mapper;
            _productsManager = productsManager;
            _categoriesManager = categoriesManager;
        }

        [ResponseType(typeof (IList<ProductModel>))]
        public async Task<IHttpActionResult> GetProducts(ODataQueryOptions<Product> query, int start, int limit,
            Guid? categoryId = null)
        {
            var queryProducts = query
                .ApplyTo(
                    _productsManager.Where(p => p.CategoryId == categoryId).OrderBy(e => e.Id).Skip(start).Take(limit))
                as
                IQueryable<Product>;

            return Ok(new StoreResult<ProductModel>
            {
                data =
                    (await queryProducts.ToListAsync()).Select(product => _mapper.Map<Product, ProductModel>(product))
                        .ToList(),
                total = _productsManager.Where(p => p.CategoryId == categoryId).Count()
            });
        }

        [ResponseType(typeof (ProductModel))]
        public async Task<IHttpActionResult> GetProduct(Guid id)
        {
            Product product = await _productsManager.FindAsync(id);

            if (product == null)
            {
                return NotFound();
            }

            ProductModel productModel = _mapper.Map<Product, ProductModel>(product);

            return Ok(productModel);
        }

        [ResponseType(typeof (ResultModel))]
        public async Task<IHttpActionResult> PutProduct(Guid id, ProductModel productModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Product product = await _productsManager.FindAsync(id);

            if (product == null)
            {
                return NotFound();
            }

            if (!StructuralComparisons.StructuralEqualityComparer
                .Equals(product.RowVersion, productModel.RowVersion))
            {
                return StatusCode(HttpStatusCode.Conflict);
            }

            _mapper.Map(productModel, product);

            try
            {
                await _productsManager.UpdateAsync(product);
            }
            catch (DatabaseException ex)
            {
                if (ex.InnerException is DbUpdateConcurrencyException)
                {
                    product = _productsManager.Find(id);

                    if (product == null)
                    {
                        return NotFound();
                    }

                    return StatusCode(HttpStatusCode.Conflict);
                }

                throw;
            }

            ResultModel resultModel = _mapper.Map<Product, ResultModel>(product);

            return Ok(resultModel);
        }

        [ResponseType(typeof (ResultModel))]
        public async Task<IHttpActionResult> PostProduct(Product product)
        {
            await _productsManager.AddAsync(product);

            ResultModel resultModel = _mapper.Map<Product, ResultModel>(product);

            return Ok(resultModel);
        }

        public async Task<IHttpActionResult> DeleteProduct(Guid id)
        {
            Product product = await _productsManager.FindAsync(id);

            if (product == null)
            {
                return NotFound();
            }

            await _productsManager.DeleteAsync(product);

            return Ok();
        }
    }
}