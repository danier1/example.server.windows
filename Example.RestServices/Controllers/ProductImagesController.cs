﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;
using Example.Domain.Entities;
using Example.Manager.Managers;

namespace Example.RestServices.Controllers
{
    public class ProductImagesController : ApiController
    {
        private readonly IProductImagesManager _productImagesManager;

        private readonly IProductsManager _productsManager;

        public ProductImagesController(IProductImagesManager productImagesManager, IProductsManager productsManager)
        {
            _productImagesManager = productImagesManager;
            _productsManager = productsManager;
        }

        public HttpResponseMessage GetProductImages(Guid id)
        {
            ProductImage productImage = _productImagesManager.Find(id);

            if (productImage == null)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }

            var ms = new MemoryStream(productImage.ImageData);
            string mediaType = string.Format("image/{0}", productImage.ImageMimeType);

            var response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new StreamContent(ms);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue(mediaType);

            return response;
        }

        public async Task<IHttpActionResult> PostProductImage()
        {
            if (!Request.Content.IsMimeMultipartContent())
            {
                return StatusCode(HttpStatusCode.UnsupportedMediaType);
            }

            var provider = new MultipartMemoryStreamProvider();
            await Request.Content.ReadAsMultipartAsync(provider);

            ProductImage image = null;
            Guid productId = Guid.Empty;

            foreach (HttpContent attr in provider.Contents)
            {
                string attrName = attr.Headers.ContentDisposition.Name.Trim('\"');

                switch (attrName)
                {
                    case "productId":
                        string productIdString = await attr.ReadAsStringAsync();

                        productId = new Guid(productIdString);

                        if (productId == Guid.Empty)
                        {
                            return BadRequest(productIdString);
                        }

                        break;
                    case "file":
                        string filename = attr.Headers.ContentDisposition.FileName.Trim('\"');
                        string extension = Path.GetExtension(filename);

                        if (extension.Length > 8)
                        {
                            return BadRequest(extension);
                        }

                        byte[] buffer = await attr.ReadAsByteArrayAsync();

                        image = new ProductImage
                        {
                            ImageData = buffer,
                            ImageMimeType = extension
                        };

                        break;
                    default:
                        return BadRequest();
                }
            }


            if (productId == Guid.Empty || image == null)
            {
                return BadRequest("Wrong params");
            }

            Product product = await _productsManager.FindAsync(productId);

            if (product == null)
            {
                return NotFound();
            }

            image.Product = product;

            await _productImagesManager.AddAsync(image);

            return Ok();
        }
    }
}