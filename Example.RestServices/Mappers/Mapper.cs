﻿using System;
using AutoMapper;

namespace Example.RestServices.Mappers
{
    public class Mapper : IMapper
    {
        private readonly IMappingEngine _mappingEngine;

        public Mapper()
        {
            AutoMapper.Mapper.Initialize(cfg => cfg.AddProfile(new ExampleProfiler()));
            _mappingEngine = AutoMapper.Mapper.Engine;
        }

        public TDestination Map<TSource, TDestination>(TSource source)
        {
            return _mappingEngine.Map<TSource, TDestination>(source);
        }

        public void Map<TSource, TDestination>(TSource source, TDestination destination)
        {
            try
            {
                _mappingEngine.Map(source, destination);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}