﻿using AutoMapper;
using Example.Domain.Entities;
using Example.RestServices.Models;

namespace Example.RestServices.Mappers
{
    public class ExampleProfiler : Profile
    {
        protected override void Configure()
        {
            AutoMapper.Mapper.CreateMap<Category, CategoryModel>()
                .ForMember(dest => dest.HasProducts, opt => opt.MapFrom(src => src.Products.Count > 0))
                .ForMember(dest => dest.HasSubCategories, opt => opt.MapFrom(src => src.SubCategories.Count > 0))
                .ReverseMap()
                .ForMember(s => s.Image, opt => opt.Ignore());


            AutoMapper.Mapper.CreateMap<Category, CategoryResultModel>()
                .ForMember(dest => dest.HasProducts, opt => opt.MapFrom(src => src.Products.Count > 0))
                .ForMember(dest => dest.HasSubCategories, opt => opt.MapFrom(src => src.SubCategories.Count > 0));

            AutoMapper.Mapper.CreateMap<Product, ProductModel>()
                .ReverseMap()
                .ForMember(s => s.Image, opt => opt.Ignore());

            AutoMapper.Mapper.CreateMap<Product, ResultModel>();
        }
    }
}