﻿namespace Example.RestServices.Mappers
{
    public interface IMapper
    {
        TDestination Map<TSource, TDestination>(TSource source);
        void Map<TSource, TDestination>(TSource source, TDestination destination);
    }
}