﻿using Example.Core.Enums;

namespace Example.RestServices.Models
{
    public class CategoryResultModel : ResultModel
    {
        public bool HasProducts { get; set; }
        public bool HasSubCategories { get; set; }
        public ChildrenType ChildrenType { get; set; }
    }
}