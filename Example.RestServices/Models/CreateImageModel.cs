﻿using System.ComponentModel.DataAnnotations;

namespace Example.RestServices.Models
{
    public class CreateImageModel
    {
        [Required]
        public byte[] ImageData { get; set; }

        [Required]
        [StringLength(8)]
        public string ImageMimeType { get; set; }
    }
}