﻿using System;
using Example.Core.Enums;

namespace Example.RestServices.Models
{
    public class CategoryModel : BaseEntityModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public ChildrenType ChildrenType { get; set; }
        public Guid? ParentCategoryId { get; set; }
        public bool HasProducts { get; set; }
        public bool HasSubCategories { get; set; }
    }
}