﻿using System;

namespace Example.RestServices.Models
{
    public class ResultModel
    {
        public Guid Id { get; set; }
        public byte[] RowVersion { get; set; }
    }
}