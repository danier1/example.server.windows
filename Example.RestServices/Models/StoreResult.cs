﻿using System.Collections.Generic;

namespace Example.RestServices.Models
{
    public class StoreResult<T>
    {
        public List<T> data { get; set; }
        public int total { get; set; }
    }
}