﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Example.RestServices.Models
{
    public class BaseEntityModel
    {
        [Required]
        public Guid Id { get; set; }

        [Required]
        public byte[] RowVersion { get; set; }
    }
}