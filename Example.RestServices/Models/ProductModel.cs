﻿using System;

namespace Example.RestServices.Models
{
    public class ProductModel : BaseEntityModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public Guid CategoryId { get; set; }
    }
}