﻿using System;
using System.ComponentModel.DataAnnotations;
using Example.Core.Enums;

namespace Example.RestServices.Models
{
    public class UpdateCategoryModel : BaseEntityModel
    {
        [Required]
        [StringLength(64)]
        public string Name { get; set; }

        [Required]
        [StringLength(256)]
        public string Description { get; set; }

        public Guid? ParentCategoryId { get; set; }

        [Required]
        public ChildrenType ChildrenType { get; set; }
    }
}