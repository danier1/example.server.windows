using System;
using System.Data.Entity;
using System.Data.Entity.Migrations.Infrastructure;
using AutoMapper;
using Example.DataAccess.Configuration;
using Example.DataAccess.Core;
using Example.DataAccess.Repositories;
using Example.Manager.Managers;
using Example.RestServices.Mappers;
using Microsoft.Practices.Unity;
using Mapper = Example.RestServices.Mappers.Mapper;

namespace Example.RestServices.App_Start
{
    /// <summary>
    ///     Specifies the Unity configuration for the main container.
    /// </summary>
    public class UnityConfig
    {
        #region Unity Container

        private static readonly Lazy<IUnityContainer> container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();
            RegisterTypes(container);
            return container;
        });

        /// <summary>
        ///     Gets the configured Unity container.
        /// </summary>
        public static IUnityContainer GetConfiguredContainer()
        {
            return container.Value;
        }

        #endregion

        /// <summary>Registers the type mappings with the Unity container.</summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>
        ///     There is no need to register concrete types such as controllers or API controllers (unless you want to
        ///     change the defaults), as Unity allows resolving a concrete type even if it was not previously registered.
        /// </remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            // DataAccess
            container.RegisterType<DbContext, ExampleDbContext>(new ContainerControlledLifetimeManager());
            container.RegisterType<IUnitOfWork, UnitOfWork>();

            container.RegisterType<ICategoriesRepository, CategoriesRepository>();
            container.RegisterType<ICategoryImagesRepository, CategoryImagesRepository>();
            container.RegisterType<IProductsRepository, ProductsRepository>();
            container.RegisterType<IProductImagesRepository, ProductImagesRepository>();

            // Mapper
            container.RegisterType<IMapper, Mapper>();
            
            // Manager
            container.RegisterType<ICategoriesManager, CategoriesManager>();
            container.RegisterType<ICategoryImagesManager, CategoryImagesManager>();
            container.RegisterType<IProductsManager, ProductsManager>();
            container.RegisterType<IProductImagesManager, ProductImagesManager>();
        }
    }
}