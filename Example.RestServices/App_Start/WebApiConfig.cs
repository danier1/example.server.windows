﻿using System.Web.Http;
using System.Web.Http.Cors;

namespace Example.RestServices
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute("DefaultApi", "api/{controller}/{id}", new {id = RouteParameter.Optional}
                );

            config.EnableCors(new EnableCorsAttribute("*", "*", "*"));
        }
    }
}