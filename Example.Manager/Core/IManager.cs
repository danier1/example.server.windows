﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Example.Domain.Core;

namespace Example.Manager.Core
{
    public interface IManager<TEntity, in TKey> where TEntity : BaseEntity<TKey>
    {
        void Add(TEntity entity);
        Task AddAsync(TEntity entity);
        Task AddAsync(TEntity entity, CancellationToken cancellationToken);

        void Add(IEnumerable<TEntity> entities);
        Task AddAsync(IEnumerable<TEntity> entities);
        Task AddAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken);

        void Update(TEntity entity);
        Task UpdateAsync(TEntity entity);
        Task UpdateAsync(TEntity entity, CancellationToken cancellationToken);

        void Update(IEnumerable<TEntity> entities);
        Task UpdateAsync(IEnumerable<TEntity> entities);
        Task UpdateAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken);

        void Delete(TEntity entity);
        Task DeleteAsync(TEntity entity);
        Task DeleteAsync(TEntity entity, CancellationToken cancellationToken);

        void Delete(IEnumerable<TEntity> entities);
        Task DeleteAsync(IEnumerable<TEntity> entities);
        Task DeleteAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken);

        TEntity Find(TKey key);
        Task<TEntity> FindAsync(TKey key);
        Task<TEntity> FindAsync(TKey key, CancellationToken cancellationToken);

        IQueryable<TEntity> Where(Expression<Func<TEntity, bool>> criteria, params string[] includes);

        TEntity Single(Expression<Func<TEntity, bool>> criteria, params string[] includes);
        Task<TEntity> SingleAsync(Expression<Func<TEntity, bool>> criteria, params string[] includes);
        Task<TEntity> SingleAsync(Expression<Func<TEntity, bool>> criteria, CancellationToken cancellationToken,
            params string[] includes);

        TEntity SingleOrDefault(Expression<Func<TEntity, bool>> criteria, params string[] includes);
        Task<TEntity> SingleOrDefaultAsync(Expression<Func<TEntity, bool>> criteria, params string[] includes);
        Task<TEntity> SingleOrDefaultAsync(Expression<Func<TEntity, bool>> criteria,
            CancellationToken cancellationToken, params string[] includes);

        TEntity First(Expression<Func<TEntity, bool>> criteria, params string[] includes);
        Task<TEntity> FirstAsync(Expression<Func<TEntity, bool>> criteria, params string[] includes);
        Task<TEntity> FirstAsync(Expression<Func<TEntity, bool>> criteria, CancellationToken cancellationToken,
            params string[] includes);

        TEntity FirstOrDefault(Expression<Func<TEntity, bool>> criteria, params string[] includes);
        Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> criteria, params string[] includes);
        Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> criteria,
            CancellationToken cancellationToken, params string[] includes);

        IList<TEntity> FindAll(Expression<Func<TEntity, bool>> criteria, params string[] includes);
        Task<IList<TEntity>> FindAllAsync(Expression<Func<TEntity, bool>> criteria, params string[] includes);
        Task<IList<TEntity>> FindAllAsync(Expression<Func<TEntity, bool>> criteria,
            CancellationToken cancellationToken, params string[] includes);

        IList<TEntity> FindAll(params string[] includes);
        Task<IList<TEntity>> FindAllAsync(params string[] includes);
        Task<IList<TEntity>> FindAllAsync(CancellationToken cancellationToken, params string[] includes);

        IList<TEntity> FindPaged(Expression<Func<TEntity, bool>> criteria, int skip, int take,
            params string[] includes);
        Task<IList<TEntity>> FindPagedAsync(Expression<Func<TEntity, bool>> criteria, int skip, int take,
            params string[] includes);
        Task<IList<TEntity>> FindPagedAsync(Expression<Func<TEntity, bool>> criteria, int skip, int take,
            CancellationToken cancellationToken, params string[] includes);

        IList<TEntity> FindPaged(int skip, int take, params string[] includes);
        Task<IList<TEntity>> FindPagedAsync(int skip, int take, params string[] includes);
        Task<IList<TEntity>> FindPagedAsync(int skip, int take, CancellationToken cancellationToken,
            params string[] includes);

        int Count();
        Task<int> CountAsync();
        Task<int> CountAsync(CancellationToken cancellationToken);
    }
}