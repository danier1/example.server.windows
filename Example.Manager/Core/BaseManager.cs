﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Example.Core.Exceptions;
using Example.DataAccess.Core;
using Example.Domain.Core;

namespace Example.Manager.Core
{
    public class BaseManager<TIRepository, TEntity, TKey> : IManager<TEntity, TKey>
        where TIRepository : IRepository<TEntity, TKey>
        where TEntity : BaseEntity<TKey>
    {
        protected readonly TIRepository Repository;
        protected readonly IUnitOfWork UnitOfWork;

        public BaseManager(TIRepository repository, IUnitOfWork unitOfWork)
        {
            Repository = repository;
            UnitOfWork = unitOfWork;
        }

        public void Add(TEntity entity)
        {
            DoAction(Repository.Add, entity);
        }

        public async Task AddAsync(TEntity entity)
        {
            await DoActionAsync(Repository.Add, entity);
        }

        public async Task AddAsync(TEntity entity, CancellationToken cancellationToken)
        {
            await DoActionAsync(Repository.Add, entity, cancellationToken);
        }

        public void Add(IEnumerable<TEntity> entities)
        {
            DoActionList(Repository.Add, entities);
        }

        public async Task AddAsync(IEnumerable<TEntity> entities)
        {
            await DoActionListAsync(Repository.Add, entities);
        }

        public async Task AddAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken)
        {
            await DoActionListAsync(Repository.Add, entities, cancellationToken);
        }

        public void Update(TEntity entity)
        {
            DoAction(Repository.Update, entity);
        }

        public async Task UpdateAsync(TEntity entity)
        {
            await DoActionAsync(Repository.Update, entity);
        }

        public async Task UpdateAsync(TEntity entity, CancellationToken cancellationToken)
        {
            await DoActionAsync(Repository.Update, entity, cancellationToken);
        }

        public void Update(IEnumerable<TEntity> entities)
        {
            DoActionList(Repository.Update, entities);
        }

        public async Task UpdateAsync(IEnumerable<TEntity> entities)
        {
            await DoActionListAsync(Repository.Update, entities);
        }

        public async Task UpdateAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken)
        {
            await DoActionListAsync(Repository.Update, entities, cancellationToken);
        }

        public void Delete(TEntity entity)
        {
            DoAction(Repository.Delete, entity);
        }

        public async Task DeleteAsync(TEntity entity)
        {
            await DoActionAsync(Repository.Delete, entity);
        }

        public async Task DeleteAsync(TEntity entity, CancellationToken cancellationToken)
        {
            await DoActionAsync(Repository.Update, entity, cancellationToken);
        }

        public void Delete(IEnumerable<TEntity> entities)
        {
            DoActionList(Repository.Delete, entities);
        }

        public async Task DeleteAsync(IEnumerable<TEntity> entities)
        {
            await DoActionListAsync(Repository.Delete, entities);
        }

        public async Task DeleteAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken)
        {
            await DoActionListAsync(Repository.Delete, entities, cancellationToken);
        }

        public TEntity Find(TKey key)
        {
            return GetEntity(Repository.Find(key));
        }

        public async Task<TEntity> FindAsync(TKey key)
        {
            return await FindAsync(key, CancellationToken.None);
        }

        public async Task<TEntity> FindAsync(TKey key, CancellationToken cancellationToken)
        {
            return await GetEntityAsync(Repository.FindAsync(key, cancellationToken), cancellationToken);
        }

        public IQueryable<TEntity> Where(Expression<Func<TEntity, bool>> criteria, params string[] includes)
        {
            return Repository.Where(criteria, includes);
        }

        public TEntity Single(Expression<Func<TEntity, bool>> criteria, params string[] includes)
        {
            return GetEntity(Repository.Where(criteria, includes).Single());
        }

        public async Task<TEntity> SingleAsync(Expression<Func<TEntity, bool>> criteria, params string[] includes)
        {
            return await SingleAsync(criteria, CancellationToken.None, includes);
        }

        public async Task<TEntity> SingleAsync(Expression<Func<TEntity, bool>> criteria,
            CancellationToken cancellationToken, params string[] includes)
        {
            return await GetEntityAsync(Repository.Where(criteria, includes).SingleAsync(cancellationToken),
                cancellationToken);
        }

        public TEntity SingleOrDefault(Expression<Func<TEntity, bool>> criteria, params string[] includes)
        {
            return GetEntity(Repository.Where(criteria, includes).SingleOrDefault());
        }

        public async Task<TEntity> SingleOrDefaultAsync(Expression<Func<TEntity, bool>> criteria,
            params string[] includes)
        {
            return await SingleOrDefaultAsync(criteria, CancellationToken.None, includes);
        }

        public async Task<TEntity> SingleOrDefaultAsync(Expression<Func<TEntity, bool>> criteria,
            CancellationToken cancellationToken, params string[] includes)
        {
            return await GetEntityAsync(Repository.Where(criteria, includes).SingleOrDefaultAsync(cancellationToken),
                cancellationToken);
        }

        public TEntity First(Expression<Func<TEntity, bool>> criteria, params string[] includes)
        {
            return GetEntity(Repository.Where(criteria, includes).First());
        }

        public async Task<TEntity> FirstAsync(Expression<Func<TEntity, bool>> criteria, params string[] includes)
        {
            return await FirstAsync(criteria, CancellationToken.None, includes);
        }

        public async Task<TEntity> FirstAsync(Expression<Func<TEntity, bool>> criteria,
            CancellationToken cancellationToken, params string[] includes)
        {
            return await GetEntityAsync(Repository.Where(criteria, includes).FirstAsync(cancellationToken),
                cancellationToken);
        }

        public TEntity FirstOrDefault(Expression<Func<TEntity, bool>> criteria, params string[] includes)
        {
            return GetEntity(Repository.Where(criteria, includes).FirstOrDefault());
        }

        public async Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> criteria,
            params string[] includes)
        {
            return await FirstOrDefaultAsync(criteria, CancellationToken.None, includes);
        }

        public async Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> criteria,
            CancellationToken cancellationToken, params string[] includes)
        {
            return await GetEntityAsync(Repository.Where(criteria, includes).FirstOrDefaultAsync(cancellationToken),
                cancellationToken);
        }

        public IList<TEntity> FindAll(Expression<Func<TEntity, bool>> criteria, params string[] includes)
        {
            return GetEntityList(Repository.Where(criteria, includes).ToList());
        }

        public async Task<IList<TEntity>> FindAllAsync(Expression<Func<TEntity, bool>> criteria,
            params string[] includes)
        {
            return await FindAllAsync(criteria, CancellationToken.None, includes);
        }

        public async Task<IList<TEntity>> FindAllAsync(Expression<Func<TEntity, bool>> criteria,
            CancellationToken cancellationToken, params string[] includes)
        {
            return await GetEntityListAsync(Repository.Where(criteria, includes).ToListAsync(cancellationToken),
                cancellationToken);
        }

        public IList<TEntity> FindAll(params string[] includes)
        {
            return GetEntityList(Repository.FindAll(includes).ToList());
        }

        public async Task<IList<TEntity>> FindAllAsync(params string[] includes)
        {
            return await FindAllAsync(CancellationToken.None, includes);
        }

        public async Task<IList<TEntity>> FindAllAsync(CancellationToken cancellationToken, params string[] includes)
        {
            return await GetEntityListAsync(Repository.FindAll(includes).ToListAsync(cancellationToken),
                cancellationToken);
        }

        public IList<TEntity> FindPaged(Expression<Func<TEntity, bool>> criteria, int skip, int take,
            params string[] includes)
        {
            return GetEntityList(Repository.Where(criteria, includes).OrderBy(e => e.Id).Skip(skip).Take(take)
                .ToList());
        }

        public async Task<IList<TEntity>> FindPagedAsync(Expression<Func<TEntity, bool>> criteria, int skip,
            int take, params string[] includes)
        {
            return await FindPagedAsync(criteria, skip, take, CancellationToken.None, includes);
        }

        public async Task<IList<TEntity>> FindPagedAsync(Expression<Func<TEntity, bool>> criteria, int skip,
            int take, CancellationToken cancellationToken, params string[] includes)
        {
            return await GetEntityListAsync(Repository.Where(criteria, includes).OrderBy(e => e.Id).Skip(skip)
                .Take(take).ToListAsync(cancellationToken), cancellationToken);
        }

        public IList<TEntity> FindPaged(int skip, int take, params string[] includes)
        {
            return GetEntityList(Repository.FindAll(includes).OrderBy(e => e.Id).Skip(skip).Take(take).ToList());
        }

        public async Task<IList<TEntity>> FindPagedAsync(int skip, int take, params string[] includes)
        {
            return await FindPagedAsync(skip, take, CancellationToken.None, includes);
        }

        public async Task<IList<TEntity>> FindPagedAsync(int skip, int take, CancellationToken cancellationToken,
            params string[] includes)
        {
            return await GetEntityListAsync(Repository.FindAll(includes).OrderBy(e => e.Id).Skip(skip).Take(take)
                .ToListAsync(cancellationToken), cancellationToken);
        }

        public int Count()
        {
            int count = 0;

            try
            {
                count = Repository.Count();
            }
            catch (Exception ex)
            {
                ManageException(ex);
            }

            return count;
        }

        public async Task<int> CountAsync()
        {
            return await CountAsync(CancellationToken.None);
        }

        public async Task<int> CountAsync(CancellationToken cancellationToken)
        {
            int count = 0;

            try
            {
                count = await Repository.CountAsync(cancellationToken);
            }
            catch (Exception ex)
            {
                ManageException(ex);
            }

            return count;
        }

        #region Private Methods

        private void DoAction(Action<TEntity> action, TEntity entity)
        {
            action(entity);

            Commit();
        }

        private async Task DoActionAsync(Action<TEntity> action, TEntity entity)
        {
            await DoActionAsync(action, entity, CancellationToken.None);
        }

        private async Task DoActionAsync(Action<TEntity> action, TEntity entity, CancellationToken cancellationToken)
        {
            action(entity);

            await CommitAsync(cancellationToken);
        }

        private void DoActionList(Action<TEntity> action, IEnumerable<TEntity> entities)
        {
            foreach (var entity in entities)
            {
                action(entity);
            }

            Commit();
        }

        private async Task DoActionListAsync(Action<TEntity> action, IEnumerable<TEntity> entities)
        {
            await DoActionListAsync(action, entities, CancellationToken.None);
        }

        private async Task DoActionListAsync(Action<TEntity> action, IEnumerable<TEntity> entities,
            CancellationToken cancellationToken)
        {
            foreach (var entity in entities)
            {
                action(entity);
            }

            await CommitAsync(cancellationToken);
        }

        private void Commit()
        {
            try
            {
                UnitOfWork.Commit();
            }
            catch (Exception ex)
            {
                ManageException(ex);
            }
        }

        private async Task CommitAsync(CancellationToken cancellationToken)
        {
            try
            {
                await UnitOfWork.CommitAsync(cancellationToken);
            }
            catch (Exception ex)
            {
                ManageException(ex);
            }
        }

        private void ManageException(Exception ex)
        {
            throw new DatabaseException(ex);
        }

        private TEntity GetEntity(TEntity method)
        {
            TEntity entity = default(TEntity);

            try
            {
                entity = method;

                UnitOfWork.Commit();
            }
            catch (Exception ex)
            {
                ManageException(ex);
            }

            return entity;
        }

        private async Task<TEntity> GetEntityAsync(Task<TEntity> methodAsync,
            CancellationToken cancellationToken = default (CancellationToken))
        {
            TEntity entity = default(TEntity);

            try
            {
                entity = await methodAsync;

                await UnitOfWork.CommitAsync(cancellationToken);
            }
            catch (Exception ex)
            {
                ManageException(ex);
            }

            return entity;
        }

        private IList<TEntity> GetEntityList(List<TEntity> method)
        {
            var entities = default(IList<TEntity>);

            try
            {
                entities = method;

                UnitOfWork.Commit();
            }
            catch (Exception ex)
            {
                ManageException(ex);
            }

            return entities;
        }

        private async Task<IList<TEntity>> GetEntityListAsync(Task<List<TEntity>> methodAsync,
            CancellationToken cancellationToken = default (CancellationToken))
        {
            var entities = default(IList<TEntity>);

            try
            {
                entities = await methodAsync;

                await UnitOfWork.CommitAsync(cancellationToken);
            }
            catch (Exception ex)
            {
                ManageException(ex);
            }

            return entities;
        }

        #endregion
    }
}