﻿using System;
using Example.DataAccess.Core;
using Example.DataAccess.Repositories;
using Example.Domain.Entities;
using Example.Manager.Core;

namespace Example.Manager.Managers
{
    public class CategoryImagesManager : BaseManager<ICategoryImagesRepository, CategoryImage, Guid>,
        ICategoryImagesManager
    {
        public CategoryImagesManager(ICategoryImagesRepository repository, IUnitOfWork unitOfWork)
            : base(repository, unitOfWork)
        {
        }
    }
}