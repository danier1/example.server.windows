﻿using System;
using Example.DataAccess.Core;
using Example.DataAccess.Repositories;
using Example.Domain.Entities;
using Example.Manager.Core;

namespace Example.Manager.Managers
{
    public class ProductImagesManager : BaseManager<IProductImagesRepository, ProductImage, Guid>,
        IProductImagesManager
    {
        public ProductImagesManager(IProductImagesRepository repository, IUnitOfWork unitOfWork)
            : base(repository, unitOfWork)
        {
        }
    }
}