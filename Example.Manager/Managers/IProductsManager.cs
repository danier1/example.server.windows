﻿using System;
using Example.Domain.Entities;
using Example.Manager.Core;

namespace Example.Manager.Managers
{
    public interface IProductsManager : IManager<Product, Guid>
    {
    }
}