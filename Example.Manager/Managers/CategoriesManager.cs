﻿using System;
using System.Threading.Tasks;
using Example.DataAccess.Core;
using Example.DataAccess.Repositories;
using Example.Domain.Entities;
using Example.Manager.Core;

namespace Example.Manager.Managers
{
    public class CategoriesManager : BaseManager<ICategoriesRepository, Category, Guid>, ICategoriesManager
    {
        public CategoriesManager(ICategoriesRepository repository, IUnitOfWork unitOfWork)
            : base(repository, unitOfWork)
        {
        }

        public async Task RecursiveDeleteAsync(Guid categoryId)
        {
            await Repository.RecursiveDeleteAsync(categoryId);
        }
    }
}