﻿using System;
using Example.DataAccess.Core;
using Example.DataAccess.Repositories;
using Example.Domain.Entities;
using Example.Manager.Core;

namespace Example.Manager.Managers
{
    public class ProductsManager : BaseManager<IProductsRepository, Product, Guid>, IProductsManager
    {
        public ProductsManager(IProductsRepository repository, IUnitOfWork unitOfWork)
            : base(repository, unitOfWork)
        {
        }
    }
}