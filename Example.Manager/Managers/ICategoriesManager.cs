﻿using System;
using System.Threading.Tasks;
using Example.Domain.Entities;
using Example.Manager.Core;

namespace Example.Manager.Managers
{
    public interface ICategoriesManager : IManager<Category, Guid>
    {
        Task RecursiveDeleteAsync(Guid categoryId);
    }
}