﻿using System;
using System.Runtime.Serialization;

namespace Example.Core.Exceptions
{
    [Serializable]
    public class DatabaseException : Exception
    {
        private const string DefaultMessage = "A database error has occurred.";

        public DatabaseException()
            : this(DefaultMessage)
        {
        }

        public DatabaseException(string message)
            : base(message)
        {
        }

        public DatabaseException(Exception innerException)
            : base(DefaultMessage, innerException)
        {
        }

        public DatabaseException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected DatabaseException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}